package server

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"time"
)

//Config holds the server config
type Config struct {
	Server Server `json:"Server"`
}

// ParseJSON unmarshals bytes to structs
func (c *Config) ParseJSON(b []byte) error {
	return json.Unmarshal(b, &c)
}

// Server stores the hostname and port number
type Server struct {
	Hostname string
	Port     int
}

// httpAddress returns the HTTP address
func (s Server) httpAddress() string {
	return s.Hostname + ":" + fmt.Sprintf("%d", s.Port)
}

func startHTTP(h http.Handler, s Server) {
	fmt.Println(time.Now().Format("2006-01-02 03:04:05 PM"), "Running on "+s.httpAddress())

	//Start the HTTP listener
	log.Fatal(http.ListenAndServe(s.httpAddress(), h))
}

//Run starts the HTTP listener
func Run(h http.Handler, s Server) {
	startHTTP(h, s)
}
